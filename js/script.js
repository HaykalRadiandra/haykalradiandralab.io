const text = document.querySelector('.sec-text');

const textLoad = () => {
    setTimeout(() => {
        text.textContent = "Haykal Radiandra";
    }, 0);
    setTimeout(() => {
        text.textContent = "Web Developer";
    }, 4000);
    setTimeout(() => {
        text.textContent = "Enthusiast Linux";
    }, 8000);
}

textLoad();
setInterval(textLoad, 12000);

// function reveal() {
//     let reveals = document.querySelectorAll(".reveal");
  
//     for (let i = 0; i < reveals.length; i++) {
//       let windowHeight = window.innerHeight;
//       let elementTop = reveals[i].getBoundingClientRect().top;
//       let elementVisible =130;
  
//       if (elementTop < windowHeight - elementVisible) {
//           reveals[i].classList.add("active");
//           reveals[i].classList.remove("hidden");
//     } else {
//         reveals[i].classList.add("hidden");
//         reveals[i].classList.remove("active");
//       }
//     }
//   }
  
//   window.addEventListener("scroll", reveal);
  

// Play Music --------

const title = document.querySelector('.title');
const prev = document.querySelector('.prev');
const playPause = document.querySelector('.playPause');
const next = document.querySelector('.next');
const audio = document.querySelector('audio');

// Create song list
const songList = [
    {
        path: "./music/Closer.mp3",
        songName: "Lagu Opening 1",
    },
    {
        path: "./music/distance.mp3",
        songName: "Lagu Opening 2",
    },
    {
        path: "./music/Ikomono_gakari.mp3",
        songName: "Lagu Opening 3",
    },
    {
        path: "./music/Kanashimi wo Yasashisa ni.mp3",
        songName: "Lagu Opening 4",
    },
];

let song_Playing = false;

// Play Song
function playSong() {
    song_Playing = true;
    audio.play();
    playPause.classList.add('active');
    // Change icon
    playPause.innerHTML = '<ion-icon name="pause-outline"></ion-icon>';
}

// Pause Song
function pauseSong() {
    song_Playing = false;
    audio.pause();
    playPause.classList.remove('active');
    // Change Icon
    playPause.innerHTML = '<ion-icon name="play-outline"></ion-icon>';
}

// Save the current state to localStorage
function saveState() {
    localStorage.setItem('songIndex', i);
    localStorage.setItem('songTime', audio.currentTime);
    localStorage.setItem('isPlaying', song_Playing);
}

// Load the current state from localStorage
function loadState() {
    const savedIndex = localStorage.getItem('songIndex');
    const savedTime = localStorage.getItem('songTime');
    const isPlaying = localStorage.getItem('isPlaying');

    if (savedIndex !== null) {
        i = parseInt(savedIndex);
        loadSong(songList[i]);
        if (savedTime !== null) {
            audio.currentTime = parseFloat(savedTime);
        }
        // Jangan otomatis memutar lagu meskipun isPlaying adalah true
        // Cukup set icon dan status
        song_Playing = false;
        playPause.classList.remove('active');
        playPause.innerHTML = '<ion-icon name="play-outline"></ion-icon>';
    }
}

// Play or pause song on click
playPause.addEventListener("click", () => (song_Playing ? pauseSong() : playSong()));

// Load song
function loadSong(song) {
    title.textContent = song.songName;
    audio.src = song.path;
}

// Current song
let i = 0;

// On load - load the state from localStorage
loadState();

// Previous song
function prevSong() {
    i--;
    if (i < 0) {
        i = songList.length - 1;
    }
    loadSong(songList[i]);
    if (song_Playing) {
        playSong();
    }
}
prev.addEventListener("click", prevSong);

// Next song
function nextSong() {
    i++;
    if (i > songList.length - 1) {
        i = 0;
    }
    loadSong(songList[i]);
    if (song_Playing) {
        playSong();
    }
}
next.addEventListener("click", nextSong);

// Automatically play the next song when the current one ends
audio.addEventListener("ended", nextSong);

// Save state before the page is unloaded
window.addEventListener("beforeunload", saveState);